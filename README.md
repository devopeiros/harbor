## Instalação do Harbor usando o Script "install.sh"
Este guia fornece instruções sobre como instalar o Harbor, um registro de contêiner de código aberto, usando o script "`install.sh`". O Harbor permite que você tenha seu próprio registro de contêiner privado, permitindo o armazenamento, distribuição e gerenciamento de suas imagens de contêiner.

Pré-requisitos
Antes de começar, certifique-se de que seu sistema atenda aos seguintes requisitos:

* Sistema Operacional: **Linux** (este guia é baseado em sistemas Linux).
* **Docker**: Certifique-se de que o Docker esteja instalado e funcionando em seu sistema.
* **Docker Compose**: Tenha o Docker Compose instalado. Se não estiver instalado, você pode seguir as instruções em https://docs.docker.com/compose/install/.
* Usuário root ou sudo: Você precisará de privilégios de root ou sudo para executar algumas etapas do processo de instalação.

### Passos de Instalação
Baixe o Código Fonte do Harbor:

Clone o repositório do `Harbor` em sua máquina usando o seguinte comando:

```
$ git clone https://gitlab.com/devopeiros/harbor.git
```

Navegue para o Diretório do Harbor:

Vá para o diretório onde o repositório do Harbor foi clonado:

```
cd harbor
```

Execute o Script "`install.sh`":

O Harbor inclui um script de instalação chamado "install.sh". Execute-o para iniciar o processo de instalação:

```
$ chmod +x install.sh
``` 
Logo após:

```
$ ./install.sh
```

O script "install.sh" solicitará informações para configurar a instalação do Harbor, como senhas, certificados SSL e configurações de banco de dados. Siga as instruções no script e forneça as informações necessárias.

Aguarde a Conclusão:

O script instalará o Harbor e configurará os serviços necessários. Aguarde até que a instalação seja concluída.

Acesso ao Harbor:

Após a conclusão da instalação, você poderá acessar o Harbor em um navegador da web usando o endereço IP ou o nome de domínio do servidor onde você o instalou. O acesso padrão é geralmente http://seu-endereco-ip/.

### Configuração Adicional
Após a instalação inicial, você pode fazer ajustes adicionais na configuração do Harbor, como configuração de autenticação, ajustes de segurança e personalização da interface. Consulte a documentação oficial do Harbor para obter informações detalhadas sobre como fazer isso: https://goharbor.io/docs/

<hr>

### Conclusão
Agora você tem o Harbor instalado e configurado usando o script "install.sh". Isso permitirá que você tenha seu próprio registro de contêiner privado para armazenar e gerenciar suas imagens de contêiner. Certifique-se de consultar a documentação oficial para obter mais informações e explorar as capacidades do Harbor.
</hr>